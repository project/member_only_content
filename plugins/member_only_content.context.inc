<?php
/**
 * @file
 * This file defines the context condition for member only content.
 */

class member_only_content_context_condition extends context_condition {

  /**
   * Condition form.
   */
  function condition_form($context) {
    $default = $this->fetch_from_context($context, 'values');
    $form = array(
      '#title' => t('Member Only Content'),
      '#description' => t('Set this context if the current page is Member Only Content for users with the specified membership status.'),
      '#type' => 'checkboxes',
      '#options' => array(
        'member' => t('User is a Member'),
        'non_member' => t('User is not a Member'),
      ),
      '#default_value' => $default,
    );

    return $form;
  }

  /**
   * Pass the context condition if the configuration is met.
   *
   * @param (stdClass) $node
   *   The current node.
   */
  function execute($node) {
    foreach ($this->get_contexts() as $context) {

      $configured_membership_statuses = $this->fetch_from_context($context, 'values');
      $user_member_status = member_only_content_is_member($node);

      $mapping = array(
        'member' => TRUE,
        'non_member' => FALSE,
      );

      foreach ($configured_membership_statuses as $status => $value) {
        if (isset($mapping[$status]) && $mapping[$status] == $user_member_status) {
          $this->condition_met($context);
          break;
        }
      }

    }
  }
}
